/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import domain.CoffeMachine;
import javax.swing.JTextField;

/**
 *
 * @author maja
 */
public final class Util {
    
    private Util() {
    }

    public static int getAmount(JTextField txtAmount) {
        try {
            return Integer.parseInt(txtAmount.getText().trim());
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public static void setCoffeMachineAmount(CoffeMachine coffeMachine, String currency, int amount) {
        coffeMachine.set(currency, coffeMachine.get(currency) + amount);
    }
    
}
