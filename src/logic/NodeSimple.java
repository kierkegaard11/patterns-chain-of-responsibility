/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import domain.CoffeMachine;
import domain.Currency;
import java.util.Map;

/**
 *
 * @author maja
 */
public class NodeSimple extends Node {

    public NodeSimple() {
        super(1, CoffeMachine.CURRENCY_1);
    }
    
    @Override
    public void getCurrency(Currency currency, Map<String, Integer> map) {
        if (controller.getCoffeMachine().get(currencyKey) <= 0) {
            throw new RuntimeException("Cannot return, missing additional currencies.");
        }
        if (currency.getAmount() >= currencyAmount) {
            map.put(currencyKey, currency.getAmount());
        } else {
            map.put(currencyKey, 0);
        }
    }
    
}
