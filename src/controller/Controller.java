/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.listeners.BtnAddActionListener;
import controller.listeners.BtnLoginActionListener;
import controller.listeners.BtnMakeCoffeeActionListener;
import controller.listeners.BtnSettingsActionListener;
import controller.listeners.BtnTakeActionListener;
import controller.listeners.JrbActionListener;
import controller.listeners.TxtDocumentListener;
import domain.CoffeMachine;
import domain.User;
import form.FormMain;
import form.FrmLogin;
import form.FrmSettings;
import java.util.List;
import observer.Subject;
import storage.StorageUser;
import util.Util;

/**
 *
 * @author maja
 */
public class Controller {

    private static Controller instance;
    
    private FormMain frmMain;
    private FrmLogin frmLogin;
    private FrmSettings frmSettings;
    private StorageUser storageUser;
    private CoffeMachine coffeMachine;

    private Controller() {
    }
    
    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }
    
    public void initialize() {
        coffeMachine = new CoffeMachine();
        storageUser = new StorageUser();
        initializeForms();
        addListeners();
        frmMain.setVisible(true);
    }

    public FormMain getFrmMain() {
        return frmMain;
    }

    public FrmLogin getFrmLogin() {
        return frmLogin;
    }

    public FrmSettings getFrmSettings() {
        return frmSettings;
    }

    public List<User> getAllUsers() {
        return storageUser.getUsers();
    }

    public CoffeMachine getCoffeMachine() {
        return coffeMachine;
    }

    private void addListeners() {
        frmMain.getBtnMake().addActionListener(new BtnMakeCoffeeActionListener());
        
        JrbActionListener jrb20ActionListener = new JrbActionListener("20");
        frmMain.getJrb20().addActionListener(jrb20ActionListener);
        jrb20ActionListener.actionPerformed(null);
        
        frmMain.getJrb25().addActionListener(new JrbActionListener("25"));
        frmMain.getJrb30().addActionListener(new JrbActionListener("30"));
        frmMain.getJrb40().addActionListener(new JrbActionListener("40"));
        frmMain.getJrb50().addActionListener(new JrbActionListener("50"));
        frmMain.getBtnSettings().addActionListener(new BtnSettingsActionListener());

        TxtDocumentListener txtActionListener = new TxtDocumentListener();
        frmMain.getTxtAmount1().getDocument().addDocumentListener(txtActionListener);
        frmMain.getTxtAmount20().getDocument().addDocumentListener(txtActionListener);
        frmMain.getTxtAmount50().getDocument().addDocumentListener(txtActionListener);
        frmMain.getTxtAmount100().getDocument().addDocumentListener(txtActionListener);

        frmLogin.getBtnLogin().addActionListener(new BtnLoginActionListener());
        frmSettings.getBtnTake().addActionListener(new BtnTakeActionListener());
        frmSettings.getBtnAdd().addActionListener(new BtnAddActionListener());
    }

    private void initializeForms() {
        frmMain = new FormMain(null, false);
        frmLogin = new FrmLogin(null, true);
        frmSettings = new FrmSettings(null, false);
        Subject.getInstance().notifyAllObservers();
    }

    public int getProvidedAmount(String currency) {
        switch (currency) {
            case CoffeMachine.CURRENCY_100:
                return Util.getAmount(frmMain.getTxtAmount100());
            case CoffeMachine.CURRENCY_50:
                return Util.getAmount(frmMain.getTxtAmount50());
            case CoffeMachine.CURRENCY_20:
                return Util.getAmount(frmMain.getTxtAmount20());
            case CoffeMachine.CURRENCY_1:
                return Util.getAmount(frmMain.getTxtAmount1());
            default:
                return 0;
        }
    }

    public int getProvidedMoney() {
        return getProvidedAmount(CoffeMachine.CURRENCY_100) * 100 +
                getProvidedAmount(CoffeMachine.CURRENCY_50) * 50 +
                getProvidedAmount(CoffeMachine.CURRENCY_20) * 20 +
                getProvidedAmount(CoffeMachine.CURRENCY_1);
    }

    public boolean isNoAmountProvided() {
        return frmMain.getTxtAmount1().getText().trim().isEmpty()
                && frmMain.getTxtAmount20().getText().trim().isEmpty()
                && frmMain.getTxtAmount50().getText().trim().isEmpty()
                && frmMain.getTxtAmount100().getText().trim().isEmpty();
    }
}
