/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.listeners;

import controller.Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author maja
 */
public class BtnSettingsActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        Controller.getInstance().getFrmLogin().setVisible(true);
    }

}
