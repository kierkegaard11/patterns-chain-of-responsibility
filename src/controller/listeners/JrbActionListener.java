/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.listeners;

import controller.Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;

/**
 *
 * @author maja
 */
public class JrbActionListener implements ActionListener {

    private String text;

    public JrbActionListener(String text) {
        this.text = text;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e == null || ((JRadioButton) e.getSource()).isSelected()) {
            Controller.getInstance().getFrmMain().getTxtPrice().setText(text);
        }
    }
}
