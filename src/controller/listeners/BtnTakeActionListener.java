/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.listeners;

import controller.Controller;
import domain.CoffeMachine;
import form.FrmSettings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import observer.Subject;
import util.Util;

/**
 *
 * @author maja
 */
public class BtnTakeActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        Controller controller = Controller.getInstance();
        
        FrmSettings frmSettings = controller.getFrmSettings();
        int amount100 = Util.getAmount(frmSettings.getTxtChange100());
        int amount50 = Util.getAmount(frmSettings.getTxtChange50());
        int amount20 = Util.getAmount(frmSettings.getTxtChange20());
        int amount1 = Util.getAmount(frmSettings.getTxtChange1());
        
        CoffeMachine coffeMachine = controller.getCoffeMachine();
        Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_100, -amount100);
        Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_50, -amount50);
        Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_20, -amount20);
        Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_1, -amount1);

        Subject.getInstance().notifyAllObservers();
    }

}
