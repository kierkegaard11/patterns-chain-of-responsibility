/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.listeners;

import controller.Controller;
import domain.CoffeMachine;
import domain.Currency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import logic.Node;
import logic.NodeComplex;
import logic.NodeNull;
import logic.NodeSimple;
import observer.Subject;
import util.Util;

/**
 *
 * @author maja
 */
public class BtnMakeCoffeeActionListener implements ActionListener {

    private Controller controller;
    private Node chain;

    public BtnMakeCoffeeActionListener() {
        controller = Controller.getInstance();
        createChain();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            checkEnoughMoney();
            acceptMoney();
            returnMoney();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(controller.getFrmMain(), ex.getMessage(), "Error:", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void acceptMoney() {
        try {
            int amount100 = controller.getProvidedAmount(CoffeMachine.CURRENCY_100);
            int amount50 = controller.getProvidedAmount(CoffeMachine.CURRENCY_50);
            int amount20 = controller.getProvidedAmount(CoffeMachine.CURRENCY_20);
            int amount1 = controller.getProvidedAmount(CoffeMachine.CURRENCY_1);
            
            CoffeMachine coffeMachine = controller.getCoffeMachine();
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_100, amount100);
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_50, amount50);
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_20, amount20);
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_1, amount1);

            Subject.getInstance().notifyAllObservers();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(controller.getFrmMain(), ex.getMessage(), "Error:", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void returnMoney() {
        Map<String, Integer> currencyAmountMap = initCurrencyAmountMap();
        try {
            int moneyToReturn = Integer.parseInt(controller.getFrmMain().getTxtReturns().getText().trim());
            chain.getCurrency(new Currency(moneyToReturn), currencyAmountMap);

            int amount100 = currencyAmountMap.get(CoffeMachine.CURRENCY_100);
            int amount50 = currencyAmountMap.get(CoffeMachine.CURRENCY_50);
            int amount20 = currencyAmountMap.get(CoffeMachine.CURRENCY_20);
            int amount1 = currencyAmountMap.get(CoffeMachine.CURRENCY_1);

            controller.getFrmMain().getTxt100().setText(amount100 + "");
            controller.getFrmMain().getTxt50().setText(amount50 + "");
            controller.getFrmMain().getTxt20().setText(amount20 + "");
            controller.getFrmMain().getTxt1().setText(amount1 + "");
            
            CoffeMachine coffeMachine = controller.getCoffeMachine();
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_100, -amount100);
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_50, -amount50);
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_20, -amount20);
            Util.setCoffeMachineAmount(coffeMachine, CoffeMachine.CURRENCY_1, -amount1);

            Subject.getInstance().notifyAllObservers();
        } catch (RuntimeException ex) {
            JOptionPane.showMessageDialog(controller.getFrmMain(), ex.getMessage(), "Error:", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void checkEnoughMoney() throws Exception {
        int providedMoney = controller.getProvidedMoney();
        int price = Integer.parseInt(controller.getFrmMain().getTxtPrice().getText().trim());

        if (providedMoney < price) {
            throw new Exception("provided money < price");
        }
    }

    private void createChain() {
        NodeComplex node100 = new NodeComplex(100, CoffeMachine.CURRENCY_100);
        NodeComplex node50 = new NodeComplex(50, CoffeMachine.CURRENCY_50);
        NodeComplex node20 = new NodeComplex(20, CoffeMachine.CURRENCY_20);
        NodeSimple node1 = new NodeSimple();
        
        node1.setNext(new NodeNull());
        node20.setNext(node1);
        node50.setNext(node20);
        node100.setNext(node50);

        chain = node100;
    }

    private Map<String, Integer> initCurrencyAmountMap() {
        return new HashMap() {
            {
                put(CoffeMachine.CURRENCY_100, 0);
                put(CoffeMachine.CURRENCY_50, 0);
                put(CoffeMachine.CURRENCY_20, 0);
                put(CoffeMachine.CURRENCY_1, 0);
            }
        };
    }
}
