/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import controller.Controller;

/**
 *
 * @author maja
 */
public class Start {

    public static void main(String[] args) {
        Controller controller = Controller.getInstance();
        controller.initialize();
    }
}
