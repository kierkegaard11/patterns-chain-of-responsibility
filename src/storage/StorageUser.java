/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import domain.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class StorageUser {
    
    private List<User> users;
    
    public StorageUser(){
        users = new ArrayList<>();
        User user1 = new User("admin1", "admin1");
        User user2 = new User("admin2", "admin2");
        users.add(user1);
        users.add(user2);
    }

    public List<User> getUsers() {
        return users;
    }
}
